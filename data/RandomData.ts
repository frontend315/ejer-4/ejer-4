interface RandomData {
    'currency': string;
    'numberrange': number;
    'text': string;
    'alphanumeric': string;
}

export const randomData: Array<RandomData> = [
    {
        "currency": "$88.24",
        "numberrange": 7,
        "text": "faucibus id, libero. Donec consectetuer mauris id sapien. Cras dolor",
        "alphanumeric": "FVV24IOT0II"
    },
    {
        "currency": "$67.99",
        "numberrange": 4,
        "text": "euismod est arcu ac orci. Ut semper pretium neque. Morbi",
        "alphanumeric": "AXH04NNT5BH"
    },
    {
        "currency": "$60.40",
        "numberrange": 2,
        "text": "lobortis quis, pede. Suspendisse dui. Fusce diam nunc, ullamcorper eu,",
        "alphanumeric": "LJZ61BRA9PK"
    },
    {
        "currency": "$56.59",
        "numberrange": 8,
        "text": "sem elit, pharetra ut, pharetra sed, hendrerit a, arcu. Sed",
        "alphanumeric": "ZED27XFT8TU"
    },
    {
        "currency": "$6.19",
        "numberrange": 7,
        "text": "faucibus orci luctus et ultrices posuere cubilia Curae Donec tincidunt.",
        "alphanumeric": "GCI56DFQ3MN"
    }
]