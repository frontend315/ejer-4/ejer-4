import { people } from "../data/people";
import { searchGeneric } from "./search";
import { sortItems } from "./sort";

const list = people.filter(
    person => searchGeneric(person, ['first_name', "ip_address"], '2')
).sort((prev, compare) => sortItems(prev, compare, 'first_name'))

console.log(list)