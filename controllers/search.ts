import { people, person } from "../data/people";

export const searchGeneric = <T>(object: T, properties: Array<keyof T>, query: string) => {
    if (query === '') {
        return false;
    } else {
        return properties.some(propertie => {
            const value = object[propertie]
            return (typeof value === 'string' || typeof value === 'number')
                && value.toString().toUpperCase().includes(query.toUpperCase());
        })
    }
}

export const search = (query:string) => {
    return people.filter(person => searchGeneric<person>(person, ['last_name', 'first_name'], query))
}

console.log(search('na'))