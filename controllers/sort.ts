import { randomData } from "../data/RandomData"

export const sortItems = <T>(prev: T, compare: T, property: keyof T): number => {
    return prev[property] > compare[property]
        ? 1
        : prev[property] < compare[property]
            ? -1
            : 0
}

const orderby = randomData.sort((prev, compare) => sortItems(prev, compare, 'alphanumeric'))

console.log(orderby)